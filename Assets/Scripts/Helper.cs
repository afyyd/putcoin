﻿using UnityEngine;

public sealed class Helper
{
    private static Helper s_helper = null;
    private Helper() { }
    public static Helper Instance
    {
        get
        {
            if (s_helper == null)
            {
                s_helper = new Helper();
            }
            return s_helper;
        }
    }

    //图片文字
    public GameObject CreateNumber(GameObject parent, int num, string imageName, float offx = 0)
    {
        return CreateNumber(parent, num.ToString(), imageName, offx);
    }
    public GameObject CreateNumber(GameObject parent, string num, string imageName, float offx = 0)
    {
        if (string.IsNullOrEmpty(num) || parent == null || string.IsNullOrEmpty(imageName))
        {
            Debug.Log("创建图片文字失败");
            return null;
        }

        GameObject obj = new GameObject();
        int layerNum = parent.layer;

        char[] chs = num.ToCharArray();
        UIAtlas atlas = Resources.Load("Res/Image/ImageAtlas", typeof(UIAtlas)) as UIAtlas;
        if (atlas == null)
        {
            Debug.Log("Res/Image/ImageAtlas" + "不存在");
            return null;
        }
        float w = 0;
        float off = 0;
        foreach (char i in chs)
        {
            if (atlas.GetSprite(imageName + "_" + i) == null)
            {
                continue;
            }

            UISprite sprite = NGUITools.AddSprite(obj, atlas, imageName + "_" + i, 3);
            sprite.gameObject.layer = layerNum;
            sprite.MakePixelPerfect();
            if (i == '1')//TMD 1实在是太窄了
            {
                off = 6;
            }
            else
            {
                off = 0;
            }

            //设置位置
            UISpriteData data = sprite.GetAtlasSprite();
            sprite.transform.position = new Vector3(w, 0, 0);
            w = w + data.width + offx * 2 + off;
            //Debug.Log(w);
        }

        obj.transform.parent = parent.transform;
        obj.transform.localScale = new Vector3(1, 1, 1);
        obj.transform.localPosition = new Vector3(-w / 2, 0, 0);
        obj.layer = layerNum;

        return obj;
    }

    //数字缩略
    public string ShortNumber(float num)
    {
        return ShortNumber((int)num);
    }
    public string ShortNumber(long num)
    {
        return ShortNumber((int)num);
    }
    public string ShortNumber(double num)
    {
        return ShortNumber((int)num);
    }
    private string ShortNumber(int num)
    {
        if (num < 10000)
        {
            return ""+num;
        }
        //万
        if (num < 100000)
        {
            return ""+((int)(num / 1000) * 10) / 10+"w";
        }
        //十万 百万
        if (num < 100000000 && num > 100000)
        {
            return ""+(int)num / 10000+"w";
        }
        //亿
        return ""+(int)(num / 10000000)+"y";
    }
}
