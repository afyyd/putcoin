﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPage : MonoBehaviour
{
    public GameObject Node_coin;//金钱
    public GameObject Node_dom;//钻石

    public GameObject Node_lev;//中间节点

    public GameObject sp_light;//跟随的光源
    public UISprite sp_cirlSlider;

    public double mSpeed = 0.00100D;

    private double endNumFlag = 0;
    private double startNumFlag = 0;

    // Use this for initialization
    void Start()
    {

    }

    void OnEnable()
    {
        PlayerManager.Instance.setLevel(0);
        refreshUI();
    }

    void Awake()
    {

    }

    void refreshUI()
    {
        Node_coin.transform.DestroyChildren();
        Node_dom.transform.DestroyChildren();
        Node_lev.transform.DestroyChildren();

        string num = Helper.Instance.ShortNumber(PlayerManager.Instance.getMoney());
        Helper.Instance.CreateNumber(Node_coin, num, "2");

        num = Helper.Instance.ShortNumber(PlayerManager.Instance.getDiamond());
        Helper.Instance.CreateNumber(Node_dom, num, "2");

        Helper.Instance.CreateNumber(Node_lev, PlayerManager.Instance.getLevel(), "1");
        //print(PlayerManager.Instance.getLevel());
    }

    public void setEndFlag()
    {
        float flag = 1;//升级倍数
        flag = flag + PlayerManager.Instance.getLevel();
        if (PlayerManager.Instance.getLevel() >= 300)
        {
            mSpeed = 0.00005D;
        }
        else if (PlayerManager.Instance.getLevel() >= 200)
        {
            mSpeed = 0.0005D;
        }
        else if (PlayerManager.Instance.getLevel() >= 100)
        {
            mSpeed = 0.001D;
        }
        else if (PlayerManager.Instance.getLevel() >= 20)
        {
            mSpeed = 0.005D;
        }

        int rand = Random.Range(75, 250);
        double randf = (double)rand;
        randf = randf / (1000 * flag);

        double left = endNumFlag - startNumFlag;//剩余经验        
        endNumFlag = randf + left;
        print("上一轮剩余的经验 = " + left + "，  当前增加经验 = " + endNumFlag);
    }

    void refreshSlider()
    {
        //sp_light的运动范围 y(-60  60) x(-60   60)
        double dt = sp_cirlSlider.fillAmount;
        startNumFlag = startNumFlag + mSpeed;
        if (dt >= 1)
        {
            //记录1圈
            dt = 0;
            var num = PlayerManager.Instance.getLevel();
            ++num;
            PlayerManager.Instance.setLevel(num);
            refreshUI();
        }

        dt = dt + mSpeed;
        sp_cirlSlider.fillAmount = (float)dt;

        float sin = Mathf.Sin((float)dt * 6.3F) * 60;
        float cos = Mathf.Cos((float)dt * 6.3F) * 60;

        //圆运动
        sp_light.transform.localPosition = new Vector3(sin, cos, 0);//半径60
    }

    // Update is called once per frame
    void Update()
    {
        if (endNumFlag <= startNumFlag)
        {
            endNumFlag = startNumFlag = 0;
            return;
        }
        refreshSlider();
    }
}
