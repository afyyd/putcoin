﻿using System.Collections.Generic;
using UnityEngine;

public sealed class CoinManager
{
    private static CoinManager s_mgr = null;
    private CoinManager() { }
    public static CoinManager Instance
    {
        get
        {
            if (s_mgr == null)
            {
                s_mgr = new CoinManager();
            }
            return s_mgr;
        }
    }

    private List<GameObject> m_objListUse = new List<GameObject>();//使用
    private List<GameObject> m_objListDsy = new List<GameObject>();//回收

    //保存每一次和板接触的coin
    private List<GameObject> m_objListMove = new List<GameObject>();//应当移动的coin

    public void clearAll()
    {
        m_objListUse.Clear();
        m_objListDsy.Clear();
    }

    public int getDsyNum()
    {
        return m_objListDsy.Count;
    }
    public int getUseNum()
    {
        return m_objListUse.Count;
    }
    public int getMoveNum()
    {
        return m_objListMove.Count;
    }

    //判断
    public bool judge()
    {
        if (m_objListDsy.Count <= 0)
        {
            return true;
        }
        return false;
    }

    //压入
    public void pushUse(GameObject t)
    {
        t.SetActive(true);
        m_objListUse.Add(t);
    }
    public void pushDsy(GameObject t)
    {
        m_objListDsy.Add(t);
    }
    public void pushMove(GameObject t)
    {
        m_objListMove.Add(t);
    }

    public bool contains(GameObject t)
    {
        return m_objListMove.Contains(t);
    }

    public List<GameObject> getMoveList()
    {
        return m_objListMove;
    }

    //弹出
    public void popUse(GameObject t)
    {
        t.SetActive(false);
        m_objListUse.Remove(t);
    }
    public GameObject popDsy(GameObject t)
    {
        if (t == null)
        {
            t = m_objListDsy[0];
            m_objListDsy.RemoveAt(0);
            return t;
        }
        m_objListDsy.Remove(t);
        return t;
    }
    public void popMove(GameObject t)
    {
        m_objListMove.Remove(t);
    }
    public void popMove(int num)
    {
        m_objListMove.RemoveAt(num);
    }
}
