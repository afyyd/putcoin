﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemPage : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    void OnEnable()
    {
        print("系统设置");
    }

    public void MusicSlider(GameObject obj)
    {
        GameObject onChild = obj.transform.Find("on").gameObject;

        if (onChild.activeInHierarchy == false)
        {
            print("打开音乐");
        }
        else
        {
            print("关闭音乐");
        }
    }

    public void EffectSlider(GameObject obj)
    {
        GameObject onChild = obj.transform.Find("on").gameObject;

        if (onChild.activeInHierarchy == false)
        {
            print("打开音效");
        }
        else
        {
            print("关闭音效");
        }
    }

    public void Btn_ContiueGame_OnClick()
    {
        Btn_Close_OnClick();
    }

    public void Btn_Help_OnClick()
    {
        print("帮助");
    }

    public void Btn_GameOver_OnClick()
    {
        print("退出游戏");
        Application.Quit();
    }

    public void Btn_Close_OnClick()
    {
        gameObject.SetActive(false);
    }
}
