﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    public GameObject mMainPage;
    public GameObject mShopPage;
    public GameObject mBigGitPage;
    public GameObject mSystemPage;

    // Use this for initialization
    void Start()
    {
        mMainPage.SetActive(true);
        mShopPage.SetActive(false);
        mBigGitPage.SetActive(false);
        mSystemPage.SetActive(false);        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Btn_Award_OnClick()
    {
        mBigGitPage.SetActive(true);
    }

    public void Btn_System_OnClick()
    {
        mSystemPage.SetActive(true);
    }

    public void Btn_Shop_OnClick()
    {
        mShopPage.SetActive(true);
    }

    public void Btn_Auto_OnClick()
    {
        //print("自动");
    }
}
