﻿public sealed class PlayerManager
{
    private static PlayerManager s_player = null;
    private PlayerManager() { }
    public static PlayerManager Instance
    {
        get
        {
            if (s_player == null)
            {
                s_player = new PlayerManager();
            }
            return s_player;
        }
    }

    //钱
    int m_money = 0;
    //钻石
    int m_diamond = 0;
    //等级
    int m_level = 0;

    public int getMoney()
    {
        return m_money;
    }
    public void setMoney(int num)
    {
        m_money = num;
    }
    public int getDiamond()
    {
        return m_diamond;
    }
    public void setDiamond(int num)
    {
        m_diamond = num;
    }
    public int getLevel()
    {
        return m_level;
    }
    public void setLevel(int num)
    {
        m_level = num;
    }


}
